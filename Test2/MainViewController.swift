//
//  SlidableViewController.swift
//  Test2
//
//  Created by Stefano Chiesa Suryanto on 4/2/16.
//  Copyright © 2016 nus.cs3217.a0126509. All rights reserved.
//

import UIKit

class SlidableViewController: UIViewController {

  @IBOutlet weak var sideView: UIView!
  @IBOutlet weak var mainView: UIView!
  
  var maxSideCenterX: CGFloat!
  var minSideCenterX: CGFloat!
  var maxMainCenterX: CGFloat!
  var minMainCenterX: CGFloat!
  var sideWidth: CGFloat!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    sideWidth = sideView.frame.width
    maxSideCenterX = sideView.center.x
    minSideCenterX = sideView.center.x - sideWidth
    maxMainCenterX = mainView.center.x
    minMainCenterX = mainView.center.x - sideWidth
    
  }
  
  var beganSideCenterX: CGFloat!
  var beganMainCenterX: CGFloat!
  var beganTouchX: CGFloat!
  
  @IBAction func screenAction(sender: AnyObject) {
    
    let currentTouchX = sender.locationInView(view).x
    
    if sender.state == .Began {
      beganSideCenterX = sideView.center.x
      beganMainCenterX = mainView.center.x
      beganTouchX = currentTouchX
    }
    
    let moveCenterX = currentTouchX - beganTouchX
    
    sideView.center.x = beganSideCenterX + moveCenterX
    mainView.center.x = beganMainCenterX + moveCenterX
    
    sideView.center.x = min(sideView.center.x, maxSideCenterX)
    sideView.center.x = max(sideView.center.x, minSideCenterX)
    
    mainView.center.x = min(mainView.center.x, maxMainCenterX)
    mainView.center.x = max(mainView.center.x, minMainCenterX)
    
    if sender.state == .Ended {
      let moveCenterX: CGFloat!
      if sideView.center.x < 0 {
        moveCenterX =  -sideWidth / 2.0 - sideView.center.x
      } else {
        moveCenterX = sideWidth / 2.0 - sideView.center.x
      }
      sideView.center.x += moveCenterX
      mainView.center.x += moveCenterX
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

